$$(document).on('DOMContentLoaded', function(){
	
	var mainView = myApp.addView('.view-main');
	
	mainView.router.loadPage("jogos.html");

});

function JogosRodadaSelectChanged() {
	if (typeof $('.rodada > option:selected').prev().val() == 'undefined') {
		$(".jogos-rodada-botao-left").addClass("disabled");
	} else {
		$(".jogos-rodada-botao-left").removeClass("disabled");
	}
	
	if (typeof $('.rodada > option:selected').next().val() == 'undefined') {
		$(".jogos-rodada-botao-right").addClass("disabled");
	} else {
		$(".jogos-rodada-botao-right").removeClass("disabled");
	}

	BuscarJogos(false);
};

function AtualizarSelectsDynamic(todasEquipes) {

	var temporada = $(".temporada").val();
	var categoria = $(".categoria").val();
	
	if (temporada == null) temporada = 0;
	if (categoria == null) categoria = 0;
	
	$.ajax({
		type: "GET",
		url: "http://clubedobotao.com/futmesapedia/ajax-select-dynamic.php",
		data: {
			temporada: temporada,
			categoria: categoria
		},		
		crossDomain: true,
		cache: false,
		success: function(result){
			var result = $.parseJSON(result);
			
			$(".equipe").html("");
			if (todasEquipes) {
				$(".equipe").html("<option value='0' selected=true>Todas Equipes</option>");
			}
			
			$.each(result.equipe, function(i, select){
				$(".equipe").append("<option value='" + select.Id + "'>" + select.Nome + "</option>");
			});
			
			$(".rodada").html("");
			$.each(result.rodada, function(i, select){
				$(".rodada").append("<option value='" + select.Id + "' " + (result.last_rodada == select.Id ? "selected=true" : "") + ">" + select.Nome + "</option>");
			});
			
			JogosRodadaSelectChanged();
		}
	});
};

function AtualizarSelects(page, canAll) {
	
	$.ajax({
		type: "GET",
		url: "http://clubedobotao.com/futmesapedia/ajax-select.php",	
		crossDomain: true,
		cache: false,
		success: function(result){
			var result = $.parseJSON(result);
			
			$(".temporada").html("");
			if (canAll) {
				$(".temporada").append("<option value='0'>Todas Temporadas</option>");
			}
			
			$.each(result.temporada, function(i, select){
				$(".temporada").append("<option value='" + select.Id + "'>" + select.Nome + "</option>");
			});
			
			$(".temporada option").first().prop('selected', 'true');
			
			$(".categoria").html("");
			if (canAll) {
				$(".categoria").append("<option value='0'>Todas Categorias</option>");
			}
			
			$.each(result.categoria, function(i, select){
				$(".categoria").append("<option value='" + select.Id + "'>" + select.Nome + "</option>");
			});
			
			$(".categoria option").first().prop('selected', 'true');

			$(".categoria-jogador").html("");
			if (canAll) {
				$(".categoria-jogador").append("<option value='0'>Todas Categorias</option>");
			}
			
			$.each(result.categoriaJogador, function(i, select){
				$(".categoria-jogador").append("<option value='" + select.Id + "'>" + select.Nome + "</option>");
			});
			
			$(".categoria-jogador option").first().prop('selected', 'true');

			if (page == "jogos" || page == "confrontos" || page == "ranking12") {
				AtualizarSelectsDynamic(!(page == "confrontos"));
			}

			if (page == "classificacao") {
				BuscarClassificacao();
			} else if (page == "ranking12") {
				BuscarRanking12();
			}
		}
	});
}

function BuscarConfrontos() {
	
	var temporada = $(".temporada").val();
	var categoria = $(".categoria").val();
	var rodada = $(".rodada").val();
	var equipe1 = $(".equipe1").val();
	var equipe2 = $(".equipe2").val();

	$.ajax({
		type: "POST",
		url: "http://clubedobotao.com/futmesapedia/confrontos/ajax-confrontos-buscar.php",
		data: {
			temporada: temporada,
			categoria: categoria,
			rodada: rodada,
			equipe1: equipe1,
			equipe2: equipe2
		},
		crossDomain: true,
		cache: false,
		success: function(result){
			var result = $.parseJSON(result);

			$("#confrontos-result").html("");
			if (result != "") {
				var field = result[0];
				field.Empates = parseInt(field.Empates)
				
				var card =  "<div class='card confrontos-div'>"
						+	"	<div class='card-content'>"
						+	"		<div class='card-content-inner'>"	
						+	"			<div class='row no-gutter'>"	
						+	"				<div class='col-25 confrontos-div-equipe1'>"
						+	"					<div class='confrontos-div-equipe'>"
						+	"						<img class='confrontos-div-equipe-logo' src='img/logo/" + field.LogoEquipe1 + "'>"
						+	"						<br>"
						+							field.NomeEquipe1
						+	"					</div>"
						+	"				</div>"		
						+	"				<div class='col-50 confrontos-div-versus'>"
						+	"					<i class='material-icons'>close</i>"
						+	"				</div>"	
						+	"				<div class='col-25 confrontos-div-equipe2'>"
						+	"					<div class='confrontos-div-equipe'>"
						+	"						<img class='confrontos-div-equipe-logo' src='img/logo/" + field.LogoEquipe2 + "'>"
						+	"						<br>"
						+							field.NomeEquipe2
						+	"					</div>"
						+	"				</div>"	
						+	"			</div>"				
						+	"		</div>"				
						+	"	</div>"				
						+   "</div>";
				
				$("#confrontos-result").append(card);		
				
					card =  "<div class='card confrontos-div'>"
						+	"	<div class='card-content'>"
						+	"		<div class='card-content-inner'>"	
						+	"			<div class='row no-gutter'>"	
						+	"				<div class='col-25 confrontos-div-equipe1'>"
						+	"					<div class='confrontos-div-equipe'>"
						+							field.Jogos
						+	"					</div>"
						+	"				</div>"		
						+	"				<div class='col-50 confrontos-div-versus'>"
						+	"					JOG"
						+	"				</div>"	
						+	"				<div class='col-25 confrontos-div-equipe2'>"
						+	"					<div class='confrontos-div-equipe'>"
						+							field.Jogos
						+	"					</div>"
						+	"				</div>"	
						+	"			</div>"				
						+	"		</div>"				
						+	"	</div>"				
						+   "</div>";
				
				$("#confrontos-result").append(card);
					
				if (field.Jogos > 0) {
					card		= "<div class='card confrontos-div'>"
							+	"	<div class='card-content'>"
							+	"		<div class='card-content-inner'>"	
							+	"			<div class='row no-gutter'>"	
							+	"				<div class='col-25 confrontos-div-equipe1'>"
							+	"					<div class='confrontos-div-equipe'>"
							+							field.Empates
							+	"					</div>"
							+	"				</div>"		
							+	"				<div class='col-50 confrontos-div-center'>"
							+	"					EMP"
							+	"				</div>"	
							+	"				<div class='col-25 confrontos-div-equipe2'>"
							+	"					<div class='confrontos-div-equipe'>"
							+							field.Empates
							+	"					</div>"
							+	"				</div>"	
							+	"			</div>"				
							+	"		</div>"				
							+	"	</div>"				
							+  "</div>";
					
					$("#confrontos-result").append(card);

					card		= "<div class='card confrontos-div'>"
							+	"	<div class='card-content'>"
							+	"		<div class='card-content-inner'>"	
							+	"			<div class='row no-gutter'>"	
							+	"				<div class='col-25 confrontos-div-equipe1'>"
							+	"					<div class='confrontos-div-equipe'>"
							+							(field.VitoriasEquipe1 * 3 + field.Empates)
							+	"					</div>"
							+	"				</div>"		
							+	"				<div class='col-50 confrontos-div-center'>"
							+	"					PON"
							+	"				</div>"	
							+	"				<div class='col-25 confrontos-div-equipe2'>"
							+	"					<div class='confrontos-div-equipe'>"
							+							(field.VitoriasEquipe2 * 3 + field.Empates)
							+	"					</div>"
							+	"				</div>"	
							+	"			</div>"				
							+	"		</div>"				
							+	"	</div>"				
							+  "</div>";
					
					$("#confrontos-result").append(card);
					
					card		= "<div class='card confrontos-div'>"
							+	"	<div class='card-content'>"
							+	"		<div class='card-content-inner'>"	
							+	"			<div class='row no-gutter'>"	
							+	"				<div class='col-25 confrontos-div-equipe1'>"
							+	"					<div class='confrontos-div-equipe'>"
							+							field.VitoriasEquipe1
							+	"					</div>"
							+	"				</div>"		
							+	"				<div class='col-50 confrontos-div-center'>"
							+	"					VIT"
							+	"				</div>"	
							+	"				<div class='col-25 confrontos-div-equipe2'>"
							+	"					<div class='confrontos-div-equipe'>"
							+							field.VitoriasEquipe2
							+	"					</div>"
							+	"				</div>"	
							+	"			</div>"				
							+	"		</div>"				
							+	"	</div>"				
							+  "</div>";
					
					$("#confrontos-result").append(card);

					card		= "<div class='card confrontos-div'>"
							+	"	<div class='card-content'>"
							+	"		<div class='card-content-inner'>"	
							+	"			<div class='row no-gutter'>"	
							+	"				<div class='col-25 confrontos-div-equipe1'>"
							+	"					<div class='confrontos-div-equipe'>"
							+							field.GolsEquipe1
							+	"					</div>"
							+	"				</div>"		
							+	"				<div class='col-50 confrontos-div-center'>"
							+	"					GOL"
							+	"				</div>"	
							+	"				<div class='col-25 confrontos-div-equipe2'>"
							+	"					<div class='confrontos-div-equipe'>"
							+							field.GolsEquipe2
							+	"					</div>"
							+	"				</div>"	
							+	"			</div>"				
							+	"		</div>"				
							+	"	</div>"				
							+  "</div>";
					
					$("#confrontos-result").append(card);
					
					card		= "<div class='card confrontos-div'>"
							+	"	<div class='card-content'>"
							+	"		<div class='card-content-inner'>"	
							+	"			<div class='row no-gutter'>"	
							+	"				<div class='col-25 confrontos-div-equipe1'>"
							+	"					<div class='confrontos-div-equipe'>"
							+							(field.GolsEquipe1 - field.GolsEquipe2)
							+	"					</div>"
							+	"				</div>"		
							+	"				<div class='col-50 confrontos-div-center'>"
							+	"					SAL"
							+	"				</div>"	
							+	"				<div class='col-25 confrontos-div-equipe2'>"
							+	"					<div class='confrontos-div-equipe'>"
							+							(field.GolsEquipe2 - field.GolsEquipe1)
							+	"					</div>"
							+	"				</div>"	
							+	"			</div>"				
							+	"		</div>"				
							+	"	</div>"				
							+  "</div>";
					
					$("#confrontos-result").append(card);
					
					card		= "<div class='card confrontos-div'>"
							+	"	<div class='card-content'>"
							+	"		<div class='card-content-inner'>"	
							+	"			<div class='row no-gutter'>"	
							+	"				<div class='col-25 confrontos-div-equipe1'>"
							+	"					<div class='confrontos-div-equipe'>"
							+							(Math.floor(field.VitoriasEquipe1 / field.Jogos * 100)) + "%"
							+	"					</div>"
							+	"				</div>"		
							+	"				<div class='col-50 confrontos-div-center'>"
							+	"					VIT"
							+	"				</div>"	
							+	"				<div class='col-25 confrontos-div-equipe2'>"
							+	"					<div class='confrontos-div-equipe'>"
							+							(Math.floor(field.VitoriasEquipe2 / field.Jogos * 100)) + "%"
							+	"					</div>"
							+	"				</div>"	
							+	"			</div>"				
							+	"		</div>"				
							+	"	</div>"				
							+  "</div>";
					
					$("#confrontos-result").append(card);
					
					card		= "<div class='card confrontos-div'>"
							+	"	<div class='card-content'>"
							+	"		<div class='card-content-inner'>"	
							+	"			<div class='row no-gutter'>"	
							+	"				<div class='col-25 confrontos-div-equipe1'>"
							+	"					<div class='confrontos-div-equipe'>"
							+							(Math.floor(field.Empates / field.Jogos * 100)) + "%"
							+	"					</div>"
							+	"				</div>"		
							+	"				<div class='col-50 confrontos-div-center'>"
							+	"					EMP"
							+	"				</div>"	
							+	"				<div class='col-25 confrontos-div-equipe2'>"
							+	"					<div class='confrontos-div-equipe'>"
							+							(Math.floor(field.Empates / field.Jogos * 100)) + "%"
							+	"					</div>"
							+	"				</div>"	
							+	"			</div>"				
							+	"		</div>"				
							+	"	</div>"				
							+  "</div>";
					
					$("#confrontos-result").append(card);
					
					card		= "<div class='card confrontos-div'>"
							+	"	<div class='card-content'>"
							+	"		<div class='card-content-inner'>"	
							+	"			<div class='row no-gutter'>"	
							+	"				<div class='col-25 confrontos-div-equipe1'>"
							+	"					<div class='confrontos-div-equipe'>"
							+							(Math.floor(field.VitoriasEquipe2 / field.Jogos * 100)) + "%"
							+	"					</div>"
							+	"				</div>"		
							+	"				<div class='col-50 confrontos-div-center'>"
							+	"					DER"
							+	"				</div>"	
							+	"				<div class='col-25 confrontos-div-equipe2'>"
							+	"					<div class='confrontos-div-equipe'>"
							+							(Math.floor(field.VitoriasEquipe1 / field.Jogos * 100)) + "%"
							+	"					</div>"
							+	"				</div>"	
							+	"			</div>"				
							+	"		</div>"				
							+	"	</div>"				
							+  "</div>";
					
					$("#confrontos-result").append(card);
					
					card		= "<div class='card confrontos-div'>"
							+	"	<div class='card-content'>"
							+	"		<div class='card-content-inner'>"	
							+	"			<div class='row no-gutter'>"	
							+	"				<div class='col-25 confrontos-div-equipe1'>"
							+	"					<div class='confrontos-div-equipe'>"
							+							(Math.floor((field.VitoriasEquipe1 * 3 + field.Empates) / field.Jogos * 100) / 100)
							+	"					</div>"
							+	"				</div>"		
							+	"				<div class='col-50 confrontos-div-center'>"
							+	"					MPG"
							+	"				</div>"	
							+	"				<div class='col-25 confrontos-div-equipe2'>"
							+	"					<div class='confrontos-div-equipe'>"
							+							(Math.floor((field.VitoriasEquipe2 * 3 + field.Empates) / field.Jogos * 100) / 100)
							+	"					</div>"
							+	"				</div>"	
							+	"			</div>"				
							+	"		</div>"				
							+	"	</div>"				
							+  "</div>";
					
					$("#confrontos-result").append(card);
					
					card		= "<div class='card confrontos-div'>"
							+	"	<div class='card-content'>"
							+	"		<div class='card-content-inner'>"	
							+	"			<div class='row no-gutter'>"	
							+	"				<div class='col-25 confrontos-div-equipe1'>"
							+	"					<div class='confrontos-div-equipe'>"
							+							(Math.floor(field.GolsEquipe1 / field.Jogos * 100) / 100)
							+	"					</div>"
							+	"				</div>"		
							+	"				<div class='col-50 confrontos-div-center'>"
							+	"					MGP"
							+	"				</div>"	
							+	"				<div class='col-25 confrontos-div-equipe2'>"
							+	"					<div class='confrontos-div-equipe'>"
							+							(Math.floor(field.GolsEquipe2 / field.Jogos * 100) / 100)
							+	"					</div>"
							+	"				</div>"	
							+	"			</div>"				
							+	"		</div>"				
							+	"	</div>"				
							+  "</div>";
					
					$("#confrontos-result").append(card);					
					
					card		= "<div class='card confrontos-div'>"
							+	"	<div class='card-content'>"
							+	"		<div class='card-content-inner'>"	
							+	"			<div class='row no-gutter'>"	
							+	"				<div class='col-25 confrontos-div-equipe1'>"
							+	"					<div class='confrontos-div-equipe'>"
							+							(Math.floor(field.GolsEquipe2 / field.Jogos * 100) / 100)
							+	"					</div>"
							+	"				</div>"		
							+	"				<div class='col-50 confrontos-div-center'>"
							+	"					MGC"
							+	"				</div>"	
							+	"				<div class='col-25 confrontos-div-equipe2'>"
							+	"					<div class='confrontos-div-equipe'>"
							+							(Math.floor(field.GolsEquipe1 / field.Jogos * 100) / 100)
							+	"					</div>"
							+	"				</div>"	
							+	"			</div>"				
							+	"		</div>"				
							+	"	</div>"				
							+  "</div>";
							
					$("#confrontos-result").append(card);		
					
					card		= "<div class='card confrontos-div'>"
							+	"	<div class='card-content'>"
							+	"		<div class='card-content-inner'>"	
							+	"			<div class='row no-gutter'>"	
							+	"				<div class='col-25 confrontos-div-equipe1'>"
							+	"					<div class='confrontos-div-equipe'>"
							+							(field.GolsEquipe2 != 0 ? Math.floor(field.GolsEquipe1 / field.GolsEquipe2 * 100) / 100 : "1")
							+	"					</div>"
							+	"				</div>"		
							+	"				<div class='col-50 confrontos-div-center'>"
							+	"					AVE"
							+	"				</div>"	
							+	"				<div class='col-25 confrontos-div-equipe2'>"
							+	"					<div class='confrontos-div-equipe'>"
							+							(field.GolsEquipe1 != 0 ? Math.floor(field.GolsEquipe2 / field.GolsEquipe1 * 100) / 100 : "1")
							+	"					</div>"
							+	"				</div>"	
							+	"			</div>"				
							+	"		</div>"				
							+	"	</div>"				
							+  "</div>";
					
					$("#confrontos-result").append(card);					
					$("#confrontos-result").append("<div id='jogos-result-cards'></div>");					
								
					BuscarJogos(true);
				}

			} else {
				$("#confrontos-result").append("<div class='card'><div class='card-content'><div class='card-content-inner'>Não foram encontrados resultados para o filtro especificado.</div></div></div>");
			}
			
			$(".confrontos-div-title").text("Confrontos - " + $(".categoria option:selected").text());
		},
		error: function() {
			$("#confrontos-result").append("Ocorreu um erro inesperado!");
		}
	});

}

function BuscarClassificacao() {

	var temporada = $(".temporada").val();
	var categoria = $(".categoria").val();

	$.ajax({
		type: "POST",
		url: "http://clubedobotao.com/futmesapedia/classificacao/ajax-classificacao-buscar.php",
		data: {
			temporada: temporada,
			categoria: categoria
		},
		crossDomain: true,
		cache: false,
		success: function(result){
			var result = $.parseJSON(result);
			
			$("#classificacao-result").html("");
			if (result != "") {
				
				$("#classificacao-result").append("<table>");
				$("#classificacao-result").append(
						"<tr>"
					+	"	<th class='numeric-cell'></th>"
					+	"	<th class='label-cell'>Equipe</th>"
					+	"	<th class='numeric-cell'>P</th>"
					+	"	<th class='numeric-cell'>J</th>"
					+	"	<th class='numeric-cell'>V</th>"
					+	"	<th class='numeric-cell'>E</th>"
					+	"	<th class='numeric-cell'>D</th>"
					+	"	<th class='numeric-cell'>PG</th>"
					+	"	<th class='numeric-cell'>PC</th>"
					+	"	<th class='numeric-cell'>SP</th>"
					+	"	<th class='numeric-cell'>APR</th>"
					+	"	<th class='numeric-cell'>VIT</th>"
					+	"	<th class='numeric-cell'>EMP</th>"
					+	"	<th class='numeric-cell'>DER</th>"
					+	"	<th class='numeric-cell'>MP</th>"
					+	"	<th class='numeric-cell'>MPG</th>"
					+	"	<th class='numeric-cell'>MPC</th>"
					+	"	<th class='numeric-cell'>AVE</th>"
					+	"</tr>"
				);
					
				$.each(result, function(i, field){
					if (field.Jogos > 0) {
						var tr = "<tr>"
							+	 "	<td class='numeric-cell'>" + (i + 1) + "</td>"
							+	 "	<td class='label-cell'><img class='classificacao-equipe-logo' src='img/logo/" + field.Logo + "'>" + field.Nome + "</td>"
							+	 "	<td class='numeric-cell'>" + field.Pontos + "</td>"
							+	 "	<td class='numeric-cell'>" + field.Jogos + "</td>"
							+	 "	<td class='numeric-cell'>" + field.Vitorias + "</td>"
							+	 "	<td class='numeric-cell'>" + field.Empates + "</td>"
							+	 "	<td class='numeric-cell'>" + field.Derrotas + "</td>"
							+	 "	<td class='numeric-cell'>" + field.PontosGanhos + "</td>"
							+	 "	<td class='numeric-cell'>" + field.PontosContra + "</td>"
							+	 "	<td class='numeric-cell'>" + field.SaldoPontos + "</td>"
							+	 "	<td class='numeric-cell'>" + Math.floor(field.Pontos / field.Jogos * 300) + "</td>"
							+	 "	<td class='numeric-cell'>" + Math.floor(field.Vitorias / field.Jogos * 100) + "</td>"
							+	 "	<td class='numeric-cell'>" + Math.floor(field.Empates / field.Jogos * 100) + "</td>"
							+	 "	<td class='numeric-cell'>" + Math.floor(field.Derrotas / field.Jogos * 100) + "</td>"
							+	 "	<td class='numeric-cell'>" + Math.floor(field.Pontos / field.Jogos * 10) / 10 + "</td>"
							+	 "	<td class='numeric-cell'>" + Math.floor(field.PontosGanhos / field.Jogos * 10) / 10 + "</td>"
							+	 "	<td class='numeric-cell'>" + Math.floor(field.PontosContra / field.Jogos * 10) / 10 + "</td>"
							+	 "	<td class='numeric-cell'>" + (field.PontosContra != 0 ? (Math.floor(field.PontosGanhos / field.PontosContra * 10) / 10) : "1") + "</td>"
							+	 "</tr>";

						$("#classificacao-result").append(tr);
					}
				});
				
				$("#classificacao-result").append("</table>");
			} else {
				$("#classificacao-result").append("Não foram encontrados resultados para o filtro especificado.");
			}
			$(".classificacao-div-title").text("Classificação - " + $(".categoria option:selected").text());
		},
		error: function() {
			$("#classificacao-result").append("Ocorreu um erro inesperado!");
		}
	});
}


function BuscarJogos(buscarEquipe2) {
	
	var temporada = $(".temporada").val();
	var categoria = $(".categoria").val();
	var rodada = buscarEquipe2 ? 0 : $(".rodada").val();
	var equipe1 = $(".equipe1").val();
	var equipe2 = buscarEquipe2 ? $(".equipe2").val() : 0;

	$.ajax({
		type: "POST",
		url: "http://clubedobotao.com/futmesapedia/jogos/ajax-buscar.php",
		data: {
			temporada: temporada,
			categoria: categoria,
			rodada: rodada,
			equipe1: equipe1,
			equipe2: equipe2
		},
		crossDomain: true,
		cache: false,
		success: function(result){
			
			var div = buscarEquipe2 ? "#confrontos-result #jogos-result-cards" : "#jogos-result-cards";
			
			var result = $.parseJSON(result);

			$(div).html("");
			if (result != "") {

				$.each(result, function(i, field){
					var card =  "<div class='card jogos-div-jogo" + ((field.Equipe1_Placar == 0 && field.Equipe2_Placar == 0) ? " disabled jogos-div-jogo-futuro" : " ") + "'>"
							+	"	<div class='card-content'>"
							+	"		<div class='card-content-inner accordion-item'>"
							+	"			<div class='row no-gutter accordion-item-toggle'>"
							+	"				<div class='col-25 jogos-div-equipe1'>"
							+	"					<div class='jogos-div-equipe'>"
							+	"						<img class='jogos-div-equipe-logo' src='img/logo/" + field.Equipe1_Logo + "'>"
							+	"						<br>"
							+							field.Equipe1_Nome
							+	"					</div>"
							+	"				</div>"
							+	"				<div class='col-50'>"
							+	"					<div class='jogos-div-data'>"
							+	"						<p class='jogos-div-data'>" + field.DataFormatada +  " " + field.HoraFormatada + "</p>"
							+	"					</div>"
							+	"					<div class='jogos-div-placar'>"
							+							field.Equipe1_Placar + " <i class='material-icons'>close</i> " + field.Equipe2_Placar
							+	"					</div>"
							+	"				</div>"
							+	"				<div class='col-25 jogos-div-equipe2'>"
							+	"					<div class='jogos-div-equipe'>"
							+	"						<img class='jogos-div-equipe-logo' src='img/logo/" + field.Equipe2_Logo + "'>"
							+	"						<br>"
							+							field.Equipe2_Nome
							+	"					</div>"
							+	"				</div>"
							+	"			</div>";
							if (field.Equipe1_Resumo != null || field.Equipe2_Resumo != null) {
						card +=	"   		<div class='accordion-item-content'>"
							+	"   			<div class='row no-gutter'>"
							+	"   				<div class='col-45 jogos-div-equipe1 jogos-div-resumo'>"
							+							(field.Equipe1_Resumo).replace(/, /g, "<br>")
							+	"					</div>"
							+	"   				<div class='col-10'>"
							+	"					</div>"
							+	"   				<div class='col-45 jogos-div-equipe2 jogos-div-resumo'>"
							+							(field.Equipe2_Resumo).replace(/, /g, "<br>")
							+	"					</div>"
							+	"				</div>"								
							+	"			</div>";
							}								
						card +=	"		</div>"								
							+	"	</div>"
							+	"</div>";
					
					$(div).append(card);
				});
			} else {
				$(div).append("<div class='card'><div class='card-content'><div class='card-content-inner'>Não foram encontrados jogos para o filtro especificado.</div></div></div>");
			}
			
			$(".jogos-div-title").text("Jogos - " + $(".categoria option:selected").text());
		},
		error: function() {
			$(div).append("Ocorreu um erro inesperado!");
		}
	});

};


function BuscarRanking12() {
	
	var categoria = $(".categoria-jogador").val();
	var equipe1 = $(".equipe1").val();

	$.ajax({
		type: "POST",
		url: "http://clubedobotao.com/futmesapedia/ranking12/ajax-buscar.php",
		data: {
			categoria: categoria,
			equipe1: equipe1
		},
		crossDomain: true,
		cache: false,
		success: function(result){
			var result = $.parseJSON(result);

			$("#ranking12-result").html("");
			if (result != "") {
				
				$("#ranking12-result").append("<table>");
				$("#ranking12-result").append(
						"<tr>"
					+	"	<th class='numeric-cell'>Atu</th>"
					+	"	<th class='numeric-cell'>Ant</th>"
					+	"	<th class='numeric-cell'>Ano</th>"
					+	"	<th class='label-cell'>Botonista</th>"
					+	"	<th class='numeric-cell'>Pontos</th>"
					+	"	<th class='label-cell'>Equipe</th>"
					+	"</tr>"
				);
					
				$.each(result, function(i, field){
					var tr = "<tr>"
						+	 "	<td class='numeric-cell'>" + field.Classificacao + "</td>"
						+	 "	<td class='numeric-cell'>" + field.ClassificacaoAnterior + "</td>"
						+	 "	<td class='numeric-cell'>" + field.ClassificacaoAno + "</td>"
						+	 "	<td class='label-cell'><img class='ranking12-logo-botonista' src='img/logo/" + field.Jogador_Logo + "'>" + field.Jogador_Nome + " - " + field.IdFederacao + "</td>"
						+	 "	<td class='numeric-cell'>" + field.Pontos + "</td>"
						+	 "	<td class='label-cell'><img class='ranking12-logo-classificacao' src='img/logo/" + field.Equipe_Logo + "'>" + field.Equipe_Nome + "</td>"
						+	 "</tr>";

					$("#ranking12-result").append(tr);
				});
				
				$("#ranking12-result").append("</table>");
			} else {
				$("#ranking12-result").append("Não foram encontrados resultados para o filtro especificado.");
			}

			$(".ranking12-div-title").text("Ranking 12 Toques - " + $(".categoria-jogador option:selected").text());
		},
		error: function() {
			$("#ranking12-result").append("Ocorreu um erro inesperado!");
		}
	});

};

$$(document).on('page:init', '.page[data-page="jogos"]', function(e) {

	$('.page[data-page="jogos"] .jogos-rodada-botao-right').click(function(){
		$('.rodada > option:selected').prop('selected', 'false').next().prop('selected', 'true');
		JogosRodadaSelectChanged();
	});

	$('.page[data-page="jogos"] .jogos-rodada-botao-left').click(function(){
		$('.rodada > option:selected').prop('selected', 'false').prev().prop('selected', 'true');
		JogosRodadaSelectChanged();
	});

	$('.page[data-page="jogos"] .rodada').change(function(){
		JogosRodadaSelectChanged();
	});

	$$('.picker-filtro').on('close', function () {
		BuscarJogos(false);
	});
	
	$('.categoria, .temporada').change(function() {
		AtualizarSelectsDynamic(true);
	});

	AtualizarSelects("jogos", false);
	
	$(".li-categoria").show();
	$(".li-categoria-jogador").hide();
	$(".li-temporada").show();
	$(".li-equipe1").show();
	$(".li-equipe2").hide();
})

$$(document).on('page:init', '.page[data-page="classificacao"]', function(e) {

	$$('.picker-filtro').on('close', function () {
		BuscarClassificacao();
	});
	
	AtualizarSelects("classificacao", false);
	
	$(".li-categoria").show();
	$(".li-categoria-jogador").hide();
	$(".li-temporada").show();
	$(".li-equipe1").hide();
	$(".li-equipe2").hide();
})

$$(document).on('page:init', '.page[data-page="confrontos"]', function(e) {

	$$('.picker-filtro').on('close', function () {
		BuscarConfrontos();
	});
	
	$('.categoria, .temporada').change(function() {
		AtualizarSelectsDynamic(true);
	});
	
	AtualizarSelects("confrontos", true);
	
	$(".li-categoria").show();
	$(".li-categoria-jogador").hide();
	$(".li-temporada").show();
	$(".li-equipe1").show();
	$(".li-equipe2").show();
	
	myApp.pickerModal('.picker-filtro');
})

$$(document).on('page:init', '.page[data-page="ranking12"]', function(e) {

	$$('.picker-filtro').on('close', function () {
		BuscarRanking12();
	});
	
	$('.categoria').change(function() {
		AtualizarSelectsDynamic(true);
	});
	
	AtualizarSelects("ranking12", true);
	
	$(".li-temporada").hide();
	$(".li-categoria").hide();
	$(".li-categoria-jogador").show();
	$(".li-equipe1").show();
	$(".li-equipe2").hide();
})